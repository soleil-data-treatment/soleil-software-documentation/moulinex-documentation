# Aide et ressources de Moulinex pour Synchrotron SOLEIL

## Résumé

- extraction quick exafs vers du TXT
- Créé à Synchrotron Soleil

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Facile (tout se passe bien), dep. Qt4 pour la GUI

## Format de données

- en entrée: NXS
- en sortie: TXT
- sur la Ruche locale